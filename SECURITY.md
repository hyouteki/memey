# Security Policy

## Supported Versions

Versions 1 and above are supported. Recommended using the latest version. Navigate to the [download link](https://github.com/Hyouteki/Memey/raw/main/Memey.apk) for the latest app.

| Version  | Supported          |
| -------- | ------------------ |
| >= 1.0   | :white_check_mark: |
| < 1.0    | :x:                |

## Reporting a Vulnerability

To report any vulnerability/issues navigate to [issues](https://github.com/Hyouteki/Memey/issues) or [discussions](https://github.com/Hyouteki/Memey/discussions).
