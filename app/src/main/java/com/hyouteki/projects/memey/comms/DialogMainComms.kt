package com.hyouteki.projects.memey.comms

interface DialogMainComms {
    fun switchToMeme() {}
    fun switchToGif() {}
    fun switchToLocalMeme() {}
}