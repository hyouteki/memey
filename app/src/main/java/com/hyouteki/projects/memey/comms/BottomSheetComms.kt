package com.hyouteki.projects.memey.comms

interface BottomSheetComms {
    fun refreshFavoriteGifActivity() {}
    fun onFavoriteGifClick() {}
}