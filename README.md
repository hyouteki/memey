![MEMEY banner image](https://github.com/Hyouteki/Memey/blob/main/memey_banner_image.jpg)

[![Download][download-shield]][download-url]&nbsp;
[![Contributors][contributors-shield]][contributors-url]&nbsp;
[![Stargazers][stars-shield]][stars-url]&nbsp;
[![MIT License][license-shield]][license-url]&nbsp;
[![Latest release][release-shield]][release-url]

[contributors-shield]: https://img.shields.io/github/contributors/hyouteki/Memey.svg?style=for-the-badge
[contributors-url]: https://github.com/hyouteki/Memey/graphs/contributors
[stars-shield]: https://img.shields.io/github/stars/hyouteki/Memey.svg?style=for-the-badge
[stars-url]: https://github.com/hyouteki/Memey/stargazers
[license-shield]: https://img.shields.io/github/license/hyouteki/Memey.svg?style=for-the-badge
[license-url]: https://github.com/hyouteki/Memey/blob/master/LICENSE.md
[download-shield]: https://img.shields.io/badge/Click-to%20download%20the%20application-purple?style=for-the-badge
[download-url]: https://github.com/Hyouteki/Memey/raw/main/Memey.apk
[release-shield]: https://img.shields.io/badge/Latest%20release-%CE%BB-pink?style=for-the-badge
[release-url]: https://github.com/Hyouteki/Memey/releases/tag/Latest

## Contents
- [Screenshots](#screenshots)
- [Highlights](#highlights)
- [Meme](#meme)
- [Gif](#gif)
- [Courtesy](#courtesy)

## Screenshots
 
<img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/random-meme-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/trending-gifs-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/search-memes-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/search-gifs-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/add-meme-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/single-meme-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/more-new.jpeg" width="246" 
height="544"> <img src="https://github.com/Hyouteki/Memey/blob/main/screenshots/settings-new.jpeg" width="246" height="544"> 

## Highlights
⭐ A goto app for enjoying __memes__ and __gifs__<br>
😎 Simple, Sleek, Cheerful & Awesome UI<br>
💯 Extensive Settings<br>
📑 Multiple layouts to choose from

## Meme
⏩ Share memes with your __hommies__<br>
🔎 Search memes by __Subreddit__<br>
♥️ Mark memes as favorite<br>
📲 Submit your own memes<br>
🔭 View memes made by other __Memey users__<br>

## Gif
⏩ Share gifs with your __hommies__<br>
📈 View __Trending gifs__<br>
♥️ Mark gifs as favorite<br>

## Courtesy 
 - [D3vd/Meme_Api](https://github.com/D3vd/Meme_Api.git) 
 - [GIPHY Developers Api](https://developers.giphy.com/)
